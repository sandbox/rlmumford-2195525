<?php

/**
 * @file
 * Contains \Drupal\dcnetwork\DCNodeFormController.
 */

namespace Drupal\dcnetwork;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\ctools\Form\AjaxFormTrait;

/**
 * Form controller for dcnetwork forms.
 */
class DCNodeFormController extends EntityForm {

  use AjaxFormTrait;

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $node = $this->entity;
    if ($this->operation == 'add') {
      $form['#title'] = $this->t('Add Distributed Content Network');
    }
    else if ($this->operation == 'edit') {
      $form['#title'] = $this->t('Edit %label', array('%label' => $node->label()));
    }

    $form['label'] = array(
      '#title' => t('Label'),
      '#type' => 'textfield',
      '#default_value' => $node->label,
      '#description' => t('The human-readable name of this network.'),
      '#require' => TRUE,
      '#size' => 30,
    );

    $form['id'] = array(
      '#type' => 'machine_name',
      '#default_value' => $node->id(),
      '#maxlength' => 32,
      '#disabled' => $this->operation != 'add',
      '#description' => t('A unique machine-readable name for this network. It must contain only lower-case letters, numbers and underscores.'),
    );
    
    $form['remote'] = [
      '#type' => 'select',
      '#options' => [
        0 => $this->t('Local'),
        1 => $this->t('Remote'),
      ],
      '#title' => $this->t('Type'),
      '#description' => $this->t('Local: This node has content stored in this drupal site and can be configured to push to and pull from other repositories.<br />Remote: This is a remote node and can be configured such that the remote service can push content to or pull content from the site.'),
      '#default_value' => $node->remote,
      '#disabled' => !$node->isNew(),
    ];
    
    // @todo: Remove and replace with a scanning service.
    $form['uuid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Universal ID'),
      '#description' => $this->t('The universal id of the node. This is used for authenticating with the remote server.'),
      '#default_value' => $node->uuid,
      '#disabled' => !$node->isNew(),
    ];
    
    if ($node->isNew()) {
      $form['uuid']['#states'] = [
        'disabled' => [
          ':input[name="remote"]' => ['value' => '0'],
        ],
      ];
    }

    $form['description'] = array(
      '#title' => t('Description'),
      '#type' => 'textarea',
      '#default_value' => $node->description,
      '#description' => t('Describe this network.'),
    );
    
    $form['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint URL'),
      '#description' => $this->t('The URL of the webservice endpoint for this node.'),
      '#default_value' => $node->url,
    ];
    
    $form['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#description' => $this->t('If set, this password will be used to gain access to the remote'),
      '#default_value' => $node->password,
      '#states' => [
        'visible' => [
          ':input[name="remote"]' => ['value' => '1'],
        ],
      ],
    ];
    
    $entity_type_options = [];
    foreach (\Drupal::entityManager()->getDefinitions() as $entity_type_id => $entity_type) {
      if ($entity_type instanceof ContentEntityType) {
        $entity_type_options[$entity_type_id] = $entity_type->getLabel();
      }
    }
    $form['entity_type'] = [
      '#title' => $this->t('Entity Type'),
      '#type' => 'select',
      '#description' => $this->t('What entity type this contains.'),
      '#empty_option' => $this->t('- Select -'),
      '#options' => $entity_type_options,
      '#default_value' => $node->entity_type,
      '#states' => [
        'visible' => [
          ':input[name="remote"]' => ['value' => '0'],
        ],
      ],
      '#ajax' => [
        'wrapper' => 'bundle-select',
        'method' => 'replace',
        'callback' => [$this, 'bundleSelectAjaxLoad'],
      ],
    ];
    
    $entity_type = $node->entity_type;
    $input = $form_state->getUserInput();
    if (!empty($input['entity_type'])) {
      $entity_type = $input['entity_type'];
    }
    
    $bundle_options = [];
    if ($entity_type) {
      $bundle_info = \Drupal::entityManager()->getBundleInfo($entity_type);
      foreach ($bundle_info as $bundle => $info) {
        $bundle_options[$bundle] = $info['label'];
      }
    }
    
    $form['bundle'] = [
      '#title' => $this->t('Bundle'),
      '#type' => 'select',
      '#options' => $bundle_options,
      '#default_value' => $node->bundle,
      '#prefix' => '<div id="bundle-select">',
      '#suffix' => '</div>',
      '#states' => [
        'visible' => [
          ':input[name="remote"]' => ['value' => '0'],
        ],
      ],
    ];
    
    // If the entity is new then we don't want to build anything else.
    if ($node->isNew()) {
      return $form;
    }
    
    $form['remotes'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Remotes'),
      '#states' => [
        'visible' => [
          ':input[name="remote"]' => ['value' => '0'],
        ],
      ],
    ];
    $form['remotes']['add'] = [
      '#type' => 'link',
      '#title' => $this->t('Add new remote'),
      '#url' => Url::fromRoute('dcnetwork.dcnode_remote_add', [
        'dcnode' => $node->id,
      ]),
      '#attributes' => $this->getAjaxButtonAttributes(),
      '#attached' => [
        'library' => [
          'core/drupal.ajax',
        ],
      ],
    ];
    $form['remotes']['remotes'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Label'),
        $this->t('Name'),
        $this->t('Push Content'),
        $this->t('Pull Content'),
        $this->t('Accept Content'),
        $this->t('Share Content'),
        $this->t('Operations'),
      ],
      '#empty' => $this->t('This Node has no remotes.'),
    ];
    foreach ($node->remotes as $name => $remote) {
      $row = [];
      $row['label'] = [
        '#markup' => $node->getRemoteNode($name)->label,
      ];
      $row['name'] = [
        '#markup' => $name,
      ];
      $row['push'] = [
        '#title' => $this->t('Push'),
        '#title_display' => 'invisible',
        '#type' => 'checkbox',
        '#default_value' => $remote['push'],
      ];
      $row['pull'] = [
        '#title' => $this->t('Pull'),
        '#title_display' => 'invisible',
        '#type' => 'checkbox',
        '#default_value' => $remote['pull'],
      ];
      $row['accept'] = [
        '#title' => $this->t('Accept Content'),
        '#title_display' => 'invisible',
        '#type' => 'checkbox',
        '#default_value' => $remote['accept'],
      ];
      $row['share'] = [
        '#title' => $this->t('Share Content'),
        '#title_display' => 'invisible',
        '#type' => 'checkbox',
        '#default_value' => $remote['share'],
      ];
      $operations = [];
      $operations['delete'] = [
        'title' => $this->t('Delete'),
        'url' => Url::fromRoute('dcnetwork.dcnode_remote_delete', [
          'dcnode' => $node->id(),
          'remote_name' => $name,
        ]),
        'attributes' => $this->getAjaxAttributes(),
      ];
      $row['operations'] = [
        '#type' => 'operations',
        '#links' => $operations,
      ];
      
      $form['remotes']['remotes'][$name] = $row;
    }

    return $form;
  }
  
  public function bundleSelectAjaxLoad(array $form, FormStateInterface $form_state) {
    return $form['bundle'];
  }
}
