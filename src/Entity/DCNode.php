<?php

/**
 * @file
 * Definition of Drupal\dcnetwork\Entity\KingdomNetwork.
 */

namespace Drupal\dcnetwork\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityAccessController;
use Drupal\Core\Entity\Annotation\ConfigEntityType;
use Drupal\Core\Annotation\Translation;

/**
 * Defines the DCNode configuration entity.
 *
 * @ConfigEntityType(
 *   id = "dcnode",
 *   label = @Translation("Network Node"),
 *   handlers = {
 *     "storage" = "Drupal\Core\Config\Entity\ConfigEntityStorage",
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHander",
 *     "form" = {
 *       "default" = "Drupal\dcnetwork\DCNodeFormController",
 *       "add" = "Drupal\dcnetwork\DCNodeFormController"
 *     }
 *   },
 *   admin_permission = "administer dcnetworks",
 *   config_prefix = "node",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   }
 * )
 */
class DCNode extends ConfigEntityBase {

  /**
   * The machine name of the network.
   *
   * @var string
   */
  public $id;

  /**
   * The uuid of the network.
   *
   * @var string
   */
  public $uuid;

  /**
   * The human-readable name of the network.
   *
   * @var string
   */
  public $label;

  /**
   * A brief description of this network.
   *
   * @var string
   */
  public $description;
  
  /**
   * Whether this is a remote node or not.
   *
   * @var boolean
   */
  public $remote;
  
  /**
   * The entity type this node works with.
   *
   * Local only.
   *
   * @var string
   */
  public $entity_type;
  
  /**
   * The entity bundle this node works with.
   *
   * Local only
   *
   * @var sting
   */
  public $bundle;

  /**
   * The REST endpoint for this network.
   *
   * @var string
   */
  public $url;

  /**
   * The push password for this network.
   *
   * @var string
   */
  public $password;
  
  /**
   * Remotes.
   *
   * @var array
   */
  public $remotes = [];
  
  /**
   * Push Nodes
   *
   * If this node is local, then these are the nodes that can push content.
   * If the node is remote, then these are the local nodes that will attempt
   * to push content.
   *
   * Each item has the following keys:
   *  - Node - the node it gets pushed to.
   *  - Mapper - (optional) A class to map content to a given.
   */
  public $pushNodes = [];
  
  /**
   * Pull Nodes
   *
   * If this node is local, then these are the remote notes that are allowed
   * to pull content. If the node is remote, then these are the local nodes
   * that will attempt to pull contnent.
   *
   * @var Drupal\dcnetwork\Entity\DCNode
   */
  public $pullNodes = [];

  /**
   * An array of settings pulled from the network.
   *
   * @var array
   */
  public $settings = array();

  /**
   * An array of local settings about this network.
   *
   * @var array
   */
  public $localSettings = array();
  
  /**
   * Get a remote.
   */
  public function getRemote($name) {
    return $this->remotes[$name];
  }
  
  /**
   * Add a remote.
   */
  public function addRemote($name, $settings) {
    $this->remotes[$name] = $settings;
  }
  
  /**
   * Remove a remote.
   */
  public function removeRemote($name) {
    unset($this->remotes[$name]);
  }
  
  /**
   * Get the remote node.
   * 
   * @return \Drupal\dcnetwork\Entity\DCNode
   */
  public function getRemoteNode($name) {
    return \Drupal::entityManager()->getStorage('dcnode')->load($name);
  }
}
