<?php

/**
 * @file
 * Contains \Drupal\dcnetwork\Plugin\Field\FieldType\DCNetworkTraceItem.
 */

namespace Drupal\dcnetwork\Plugin\Field\FieldType;

use Drupal\Core\Field\ConfigFieldItemBase;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Defines the 'dcnetwork_trace' entity field type.
 *
 * @FieldType(
 *   id = "dcnetwork_trace",
 *   label = @Translation("Network Trace"),
 *   description = @Translation("An entity field containing the owner and source of a piece of content in the DC Network."),
 *   list_class = "\Drupal\dcnetwork\Plugin\Field\FieldType\DCNetworkTraceItemList"
 * )
 */
class DCNetworkTraceItem extends ConfigFieldItemBase {

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    parent::preSave();

    // If this is completely new, set this site to be the owner.
    if (empty($this->owner)) {
      $this->owner = \Drupal::config('system.site')->get('uuid');
    }

    // Likewise, if the source is not set, set the source to be this site.
    if (empty($this->source)) {
      $this->source = \Drupal::config('system.site')->get('uuid');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldDefinitionInterface $field_definition) {
    return array(
      'columns' => array(
        'owner' => array(
          'description' => 'Universal unique identifier of the site that owns this content.',
          'type' => 'varchar',
          'length' => 128,
        ),
        'source' => array(
          'description' => 'Universal unique identifier of the site this content was pulled from (blank if owned by this site).',
          'type' => 'varchar',
          'length' => 128,
        ),
      ),
    );
  }
}
