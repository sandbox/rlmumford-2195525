<?php

/**
 * @file
 * Contains \Drupal\dcnetwork\Plugin\Field\FieldType\DCNetworkTraceItemList.
 */

namespace Drupal\dcnetwork\Plugin\Field\FieldType;

use Drupal\Core\Field\ConfigFieldItemList;

/**
 * Represents a configurable entity network trace field.
 */
class DCNetworkTraceItemList extends ConfigFieldItemList {

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    $this->delegateMethod('presave');

    // Filter out empty items.
    $this->filterEmptyItems();
  }
}
