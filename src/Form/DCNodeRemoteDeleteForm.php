<?php

/**
 * @file
 * Contains \Drupal\dcnetwork\Form\DCNodeRemoteDeleteForm.
 */

namespace Drupal\dcnetwork\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\dcnetwork\Entity\DCNode;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Url;

/**
 * @todo.
 */
class DCNodeRemoteDeleteForm extends ConfirmFormBase {

  /**
   * The dcnode entity this remote is on.
   *
   * @var \Drupal\dcnetwork\Entity\DCNode
   */
  protected $node;

  /**
   * The remote info
   *
   * @var array
   */
  protected $remote;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dcnetwork_selection_condition_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the %name remote?', ['%name' => $this->node->getRemoteNode($this->remote_name)->label]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('dcnetwork.dcnode_edit', [
      'dcnode' => $this->node->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, DCNode $dcnode = NULL, $remote_name = '') {
    $this->node = $dcnode;
    $this->remote = $this->node->getRemote($remote_name);
    $this->remote_name = $remote_name;
    dpm([$this->node, $this->remote, $remote_name]);
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->node->removeRemote($this->remote_name);
    $this->node->save();
    drupal_set_message($this->t('The %name remote has been removed.', ['%name' => $this->node->getRemoteNode($this->remote_name)->label]));
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
