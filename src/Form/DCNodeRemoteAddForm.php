<?php

/**
 * @file
 * Contains \Drupal\dcnetwork\Form\DCNodeRemoteAddForm.
 */

namespace Drupal\dcnetwork\Form;

use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\Entity;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dcnetwork\Entity\DCNode;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for adding a new static context.
 */
class DCNodeRemoteAddForm extends FormBase {

  /**
   * The dcnode entity this static context belongs to.
   *
   * @var \Drupal\dcnetwork\DCNode
   */
  protected $node;
  
  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;
  
  /**
   * The remote.
   *
   * @var array
   */
   protected $remote;
  
  /**
   * Construct a new DCNodeRemoteAddForm.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   */
  public function __construct(EntityManagerInterface $entity_manager) {
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager')
    );
  }
  
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, DCNode $dcnode = NULL, $remote_name = '') {
    $this->node = $dcnode;
    $this->remote = !empty($remote_name) ? $this->node->remotes[$remote_name] : [];
    
    $node_options = [];
    foreach ($this->entityManager->getStorage('dcnode')->loadByProperties(['remote' => 1]) as $node) {
      if (!isset($this->node->remotes[$node->id])) {
        $node_options[$node->id] = $node->label;
      }
    }
    
    $form['node'] = [
      '#type' => 'select',
      '#title' => $this->t('Network Node'),
      '#options' => $node_options,
      '#default_value' => $remote_name,
    ];
    
    $form['operations'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Operations'),
      '#tree' => FALSE,
    ];
    $form['operations']['pull'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Pull content from remote'),
      '#description' => $this->t('Whether to periodically pull new content from the remote node.'),
      '#default_value' => !empty($this->remote['pull']),
    ];
    $form['operations']['push'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Push content to remote'),
      '#description' => $this->t('Whether to push new content to the remote.'),
      '#default_value' => !empty($this->remote['push']),
    ];    
    $form['operations']['accept'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Accept content from remote'),
      '#description' => $this->t('Allow this remote node to push content to this node.'),
      '#default_value' => !empty($this->remote['accept']),
    ];
    $form['operations']['share'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Share content with remote'),
      '#description' => $this->t('Allow this remote node to pull content from this node.'),
      '#default_value' => !empty($this->remote['share']),
    ];
    
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->submitButtonText(),
      '#button_type' => 'primary',
    ];
    
    return $form;
  }
  
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $remote = array(
      'node' => $form_state->getValue('node'),
      'push' => $form_state->getValue('pull'),
      'pull' => $form_state->getValue('push'),
      'accept' => $form_state->getValue('accept'),
      'share' => $form_state->getValue('share'),
    );
    
    $this->node->addRemote($form_state->getValue('node'), $remote);
    $this->node->save();
    
    // Set the submission message.
    drupal_set_message($this->submitMessageText());
    
    $form_state->setRedirectUrl('/admin/networks/node/manage/'.$this->node->id);
  }
  
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dcnetwork_node_remote_add_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function submitButtonText() {
    return $this->t('Add Remote');
  }

  /**
   * {@inheritdoc}
   */
  protected function submitMessageText() {
    return $this->t('%label has been added as a remote.', ['%label' => $this->remote['node']->label]);
  }

}