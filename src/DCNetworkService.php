<?php
/**
 * @file
 * Contains \Drupal\dcnetwork\DCNetworkService
 */

namespace Drupal\dcnetwork;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Request;
use Symfony\Component\Serializer\SerializerInterface;

class DCNetworkService {

  /**
   * The HTTP Client
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The Serializer Service
   *
   * @var \Symfony\Component\Serializer\SerializerInterface
   */
  protected $httpClient;

  /**
   * Construct a new DCNetwork Service Object.
   *
   * @param \GuzzleHttp\Client $http_client
   *   The http client service.
   */
  public function __construct(ClientInterface $http_client, SerializerInterface $serializer) {
    $this->httpClient = $http_client;
    $this->serializer = $serializer;
  }

  /**
   * Push a piece of content from a local node to a remote node.
   *
   * @param \Drupal\dcnetwork\DCNode $local
   *   The local node.
   * @param \Drupal\dcnetwork\DCNode $remote
   *   The remote node.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to be pushed.
   */
  public function doPush(DCNode $local, DCNode $remote, EntityInterface $entity) {
    // First check that we can push to this remote by doing a simple http
    // request with the remote uuid and password in the headers. We also send
    // the Entity UUID as sometimes the remote will already have this piece of
    // content and want to refuse updates.
    $headers = [
      'X-Source-Node' => $local->uuid(),
      'X-Target-Node' => $remote->uuid(),
      'X-Content-Id' => $entity->uuid(),
      'X-Password' => $remote->password,
      'content-type' => 'application/json',
    ];
    $request = new Request(
      'POST',
      $remote->url,
      $headers,
      $this->serializer->serialize($entity, 'json')
    );
    $response = $this->httpClient->send($request);
  }

  /**
   * Accept a piece of content from a remote node.
   */
  public function doAccept(Request $request) {

  }
}
