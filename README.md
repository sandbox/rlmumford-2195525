kingdom
=======

Kingdom Module for Drupal

Linking Two Sites
=================

To start sharing content, you must first create a local node, to manage the
content that exists on your website.

Pushing Content
===============

Find the Node information of the node you want to push to add a 'Remote Node'
to your site, configure this as a remote on your local node. Some remotes also
require you to register your node with them before you can push content.